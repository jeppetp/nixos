{ colors ? import ./colors.nix }:

{
  enable = true;
  shellInit = ''
    # this is WIP
    set -u fish_color_cancel "red"
    set -u fish_color_command "blue"
    set -u fish_color_comment "${colors.color9}"
    set -u fish_color_end "red"
    set -u fish_color_error "${colors.color9}"
    set -u fish_color_escape "green"
    set -u fish_color_normal "${colors.foreground}"
    set -u fish_color_operator "${colors.color8}"
    set -u fish_color_param "${colors.color7}"
    set -u fish_color_quote "${colors.color11}"
    set -u fish_color_redirection "${colors.foreground}"
    set -u fish_color_valid_path "blue"
    set -u fish_greeting ""

    # set up direnv for fish
    eval (direnv hook fish)

    alias ls "lsd"
    alias vim "nvim"
    alias v "nvim"
    alias e "emacsclient -c -nw"
    alias ns "nix-shell --command \"fish\" -p"
    alias cat "bat -p"
    alias timer "chromium cstimer.net --kiosk"

    function cd --argument dir
        if [ "dir" = "" ]
            builtin cd $HOME
        else
            builtin cd $dir
        end
    end
  '';
}
