{ pkgs, colors ? import ./colors.nix }:

let
  base00 = colors.color0;
  base01 = colors.color0;
  base02 = colors.color1;
  base03 = colors.color2;
  base04 = colors.color3;
  base05 = colors.color4;
  base06 = colors.color5;
  base07 = colors.color6;
  base08 = colors.color7;
  base09 = colors.color8;
  base0A = colors.color9;
  base0B = colors.color10;
  base0C = colors.color11;
  base0D = colors.color12;
  base0E = colors.color13;
  base0F = colors.color14;
in {
  enable = true;
  package = pkgs.qutebrowser.override {
    # this fixes a bug present on nixos unstable
    # as of Wed  8 Nov 11:56:07 CET 2023
    # Issue tracking this problem: https://github.com/NixOS/nixpkgs/issues/264668
    enableVulkan = false;
  };
  settings = {

    qt.highdpi = true;

    fonts.default_family = "Monocraft Nerd Font";

    colors = {
      # base16-qutebrowser (https://github.com/theova/base16-qutebrowser)

      # Text color of the completion widget. May be a single color to use for
      # all columns or a list of three colors, one for each column.
      completion.fg = base05;

      # Background color of the completion widget for odd rows.
      completion.odd.bg = base00;

      # Background color of the completion widget for even rows.
      completion.even.bg = base00;

      # Foreground color of completion widget category headers.
      completion.category.fg = base0D;

      # Background color of the completion widget category headers.
      completion.category.bg = base00;

      # Top border color of the completion widget category headers.
      completion.category.border.top = base00;

      # Bottom border color of the completion widget category headers.
      completion.category.border.bottom = base00;

      # Foreground color of the selected completion item.
      completion.item.selected.fg = base00;

      # Background color of the selected completion item.
      completion.item.selected.bg = base0D;

      # Top border color of the selected completion item
      completion.item.selected.border.top = base0D;

      # Bottom border color of the selected completion item.
      completion.item.selected.border.bottom = base0D;

      # Foreground color of the matched text in the selected completion item.
      completion.item.selected.match.fg = base00;

      # Foreground color of the matched text in the completion.
      completion.match.fg = base09;

      # Color of the scrollbar handle in the completion view.
      completion.scrollbar.fg = base05;

      # Color of the scrollbar in the completion view.
      completion.scrollbar.bg = base00;

      # Background color of the context menu. If set to null, the Qt default is used.
      contextmenu.menu.bg = base00;

      # Foreground color of the context menu. If set to null, the Qt default is used.
      contextmenu.menu.fg = base05;

      # Background color of the context menu’s selected item. If set to null, the Qt default is used.
      contextmenu.selected.bg = base0D;

      #Foreground color of the context menu’s selected item. If set to null, the Qt default is used.
      contextmenu.selected.fg = base00;

      # Background color for the download bar.
      downloads.bar.bg = base00;

      # Color gradient start for download text.
      downloads.start.fg = base00;

      # Color gradient start for download backgrounds.
      downloads.start.bg = base0D;

      # Color gradient end for download text.
      downloads.stop.fg = base00;

      # Color gradient stop for download backgrounds.
      downloads.stop.bg = base0C;

      # Foreground color for downloads with errors.
      downloads.error.fg = base08;

      # Font color for hints.
      hints.fg = base00;

      # Background color for hints. Note that you can use a `rgba(...)` value
      # for transparency.
      hints.bg = base0A;

      # Font color for the matched part of hints.
      hints.match.fg = base05;

      # Text color for the keyhint widget.
      keyhint.fg = base05;

      # Highlight color for keys to complete the current keychain.;
      keyhint.suffix.fg = base05;

      # Background color of the keyhint widget.
      keyhint.bg = base00;

      # Foreground color of an error message.
      messages.error.fg = base00;

      # Background color of an error message.
      messages.error.bg = base08;

      # Border color of an error message.
      messages.error.border = base08;

      # Foreground color of a warning message.
      messages.warning.fg = base00;

      # Background color of a warning message.
      messages.warning.bg = base0E;

      # Border color of a warning message.
      messages.warning.border = base0E;

      # Foreground color of an info message.
      messages.info.fg = base05;

      # Background color of an info message.
      messages.info.bg = base00;

      # Border color of an info message.
      messages.info.border = base00;

      # Foreground color for prompts.
      prompts.fg = base05;

      # Border used around UI elements in prompts.
      prompts.border = base00;

      # Background color for prompts.
      prompts.bg = base00;

      # Background color for the selected item in filename prompts.
      prompts.selected.bg = base0A;

      # Foreground color of the statusbar.
      statusbar.normal.fg = base05;

      # Background color of the statusbar.
      statusbar.normal.bg = base00;

      # Foreground color of the statusbar in insert mode.
      statusbar.insert.fg = base0C;

      # Background color of the statusbar in insert mode.
      statusbar.insert.bg = base00;

      # Foreground color of the statusbar in passthrough mode.
      statusbar.passthrough.fg = base0A;

      # Background color of the statusbar in passthrough mode.
      statusbar.passthrough.bg = base00;

      # Foreground color of the statusbar in private browsing mode.
      statusbar.private.fg = base0E;

      # Background color of the statusbar in private browsing mode.
      statusbar.private.bg = base00;

      # Foreground color of the statusbar in command mode.
      statusbar.command.fg = base04;

      # Background color of the statusbar in command mode.
      statusbar.command.bg = base01;

      # Foreground color of the statusbar in private browsing + command mode.
      statusbar.command.private.fg = base0E;

      # Background color of the statusbar in private browsing + command mode.
      statusbar.command.private.bg = base01;

      # Foreground color of the statusbar in caret mode.
      statusbar.caret.fg = base0D;

      # Background color of the statusbar in caret mode.
      statusbar.caret.bg = base00;

      # Foreground color of the statusbar in caret mode with a selection.
      statusbar.caret.selection.fg = base0D;

      # Background color of the statusbar in caret mode with a selection.
      statusbar.caret.selection.bg = base00;

      # Background color of the progress bar.
      statusbar.progress.bg = base0D;

      # Default foreground color of the URL in the statusbar.
      statusbar.url.fg = base05;

      # Foreground color of the URL in the statusbar on error.
      statusbar.url.error.fg = base08;

      # Foreground color of the URL in the statusbar for hovered links.
      statusbar.url.hover.fg = base09;

      # Foreground color of the URL in the statusbar on successful load (http).
      statusbar.url.success.http.fg = base0B;

      # Foreground color of the URL in the statusbar on successful load (https).
      statusbar.url.success.https.fg = base0B;

      # Foreground color of the URL in the statusbar when there's a warning.
      statusbar.url.warn.fg = base0E;

      # Background color of the tab bar.
      tabs.bar.bg = base00;

      # Color gradient start for the tab indicator.
      tabs.indicator.start = base0D;

      # Color gradient end for the tab indicator.
      tabs.indicator.stop = base0C;

      # Color for the tab indicator on errors.
      tabs.indicator.error = base08;

      # Foreground color of unselected odd tabs.
      tabs.odd.fg = base05;

      # Background color of unselected odd tabs.
      tabs.odd.bg = base00;

      # Foreground color of unselected even tabs.
      tabs.even.fg = base05;

      # Background color of unselected even tabs.
      tabs.even.bg = base00;

      # Background color of pinned unselected even tabs.
      tabs.pinned.even.bg = base0B;

      # Foreground color of pinned unselected even tabs.
      tabs.pinned.even.fg = base00;

      # Background color of pinned unselected odd tabs.
      tabs.pinned.odd.bg = base0B;

      # Foreground color of pinned unselected odd tabs.
      tabs.pinned.odd.fg = base00;

      # Background color of pinned selected even tabs.
      tabs.pinned.selected.even.bg = base0D;

      # Foreground color of pinned selected even tabs.
      tabs.pinned.selected.even.fg = base00;

      # Background color of pinned selected odd tabs.
      tabs.pinned.selected.odd.bg = base0D;

      # Foreground color of pinned selected odd tabs.
      tabs.pinned.selected.odd.fg = base00;

      # Foreground color of selected odd tabs.
      tabs.selected.odd.fg = base00;

      # Background color of selected odd tabs.
      tabs.selected.odd.bg = base0D;

      # Foreground color of selected even tabs.
      tabs.selected.even.fg = base00;

      # Background color of selected even tabs.
      tabs.selected.even.bg = base0D;

      # Background color for webpages if unset
      webpage.bg = "white";

      # Use the prefers_colorscheme dark header
      # XXX: this gives a warning in latest qutebrowser
      webpage.preferred_color_scheme = "dark";
      # webpage.darkmode.enabled = true;
    };
  };
}
