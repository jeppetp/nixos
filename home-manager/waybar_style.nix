{ colors ? import ./colors.nix }:

''
    * {
      border: none;
      border-radius: 0;
      /* `otf-font-awesome` is required to be installed for icons */
      font-family: Monocraft Nerd Font, FiraMono Nerd Font;
      font-size: 30px;
      min-height: 40px;
    }

    /*
    window#waybar {
        background: ${colors.background};
        color: ${colors.foreground};
        transition-property: background-color;
        transition-duration: .5s;
    }
    */

    window#waybar {
        margin-left: 20px;
        margin-right: 20px;
        margin-top: 20px;
        margin-bottom: 0px;
        background: rgba(0, 0, 0, 0.5);
        color: ${colors.foreground};
        transition-property: background-color;
        transition-duration: .5s;
    }

    window > box {
    }
    tooltip {
        background: rgba(0, 0, 0, 0.5);
        color: ${colors.foreground};
        transition-property: background-color;
        transition-duration: .5s;
        border-radius: 20px;
    }

    #workspaces {
        font-size: 70px;
        padding-left: 3px;
        padding-top: 2px;
        background: transparent;
        border-radius: 20px;
    }

    #workspaces button {
        padding-left: 4px;
        padding-right: 7px;
        background: transparent;
        color: rgba(255, 255, 255, 0.5);
        border-bottom: 3px solid transparent;
        min-width: 35px;
    }

    #workspaces button.empty {
        color: rgba(255,255,255,0.1)
    }

    #workspaces button.active {
        color: rgba(255,255,255,1);
    }

    #workspaces button.urgent {
        color: rgba(255,80,80,0.5);
    }


    #mode {
        background-color: ${colors.color1};
        border-bottom: 3px solid ${colors.foreground};
    }

    #clock,
    #backlight,
    #network,
    #custom-pipewire,
    #custom-media,
    #tray,
    #mode,
    #idle_inhibitor,
    #mpd {
        padding: 0 10px;
        margin: 0 4px;
        color: ${colors.foreground};
    }

    /* BATTERY */
    #battery {
        background: transparent;
    }

    @keyframes blink {
        to {
            color: ${colors.foreground};
        }
    }

    #battery.critical:not(.charging) {
        color: rgb(255,80,80);
        animation-name: blink;
        animation-duration: 0.5s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        animation-direction: alternate;
    }

    #battery.good {
        color: rgb(80,255,80)
  }

    /* SOMETHING */
    label:focus {
        background-color: ${colors.color4};
    }

    #network.disconnected {
        color: rgb(255,80,80);
    }

    #custom-pipewire.muted {
        color: ${colors.color1};
    }

    /* PERFORMANCE STATS */
    #cpu,
    #temperature,
    #memory {
        padding: 0 10px;
        margin: 0 4px;
    }
    #temperature.critical,
    #cpu.critical,
    #memory.critical {
        background-color: ${colors.color1};
    }

    #idle_inhibitor.activated {
        background-color: ${colors.color8};
        color: ${colors.foreground};
    }

    /* not used
    #custom-media {
        background-color: #66cc99;
        color: #2a5c45;
        min-width: 100px;
    }

    #custom-media.custom-spotify {
        background-color: #66cc99;
    }

    #tray {
        background-color: #2980b9;
    }

    #custom-media.custom-vlc {
        background-color: #ffa000;
    }

    #mpd {
        background-color: #66cc99;
        color: #2a5c45;
    }

    #mpd.disconnected {
        background-color: #f53c3c;
    }

    #mpd.stopped {
        background-color: #90b1b1;
    }

    #mpd.paused {
        background-color: #51a37a;
    }

    */
''
