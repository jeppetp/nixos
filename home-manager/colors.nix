rec {
  # special
  foreground = "#f0f0f0";
  background = "#191919";
  cursorColor = color1;
  highlight = color9;
  darkHighlight = color1;

  # black
  color0 = "#191919";
  color8 = "#4c4c4c";
  black = "#000000";

  # red
  color1 = "#cc4c4c";
  color9 = "#f2b2cc";

  # green
  color2 = "#57a64e";
  color10 = "#7fcc19";

  # yellow
  color3 = "#f2b233";
  color11 = "#dede6c";

  # blue
  color4 = "#3366cc";
  color12 = "#99b2f2";

  # magenta
  color5 = "#b266e5";
  color13 = "#e57fd8";

  # cyan
  color6 = "#4c99b2";
  color14 = "#99b2f2";

  # white
  color7 = "#999999";
  color15 = "#f0f0f0";
}
