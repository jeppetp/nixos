{ pkgs ? import <nixpkgs> { } }:

{
  enable = true;
  settings = {
    global = {
      browser = "${pkgs.qutebrowser}/bin/qutebrowser";
      padding = 10;
      border = 50;
      follow = "mouse";
      font = "FiraCode Nerd Font 25";
      width = 500;
      height = 1000;
      scale = 2;
      origin = "top-left";
      background = "#00000030";
      font-size = "64";
      corner_radius = "10";
    };
  };
}
