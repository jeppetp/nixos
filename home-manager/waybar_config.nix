{ pkgs, colors ? import ./colors.nix }:

''
  {
    "layer": "top",
    "position": "top",
    "height": 60,
    "modules-left": ["hyprland/workspaces", "hyprland/mode", "custom/media"],
    "modules-center": ["hyprland/window"],
    "modules-right": ["idle_inhibitor", "backlight", "custom/pipewire", "network", "cpu", "memory", "temperature", "battery", "clock", "tray"],
    // Modules configuration
    "hyprland/workspaces": {
      "disable-scroll": true,
      "all-outputs": false,
      "format": "{icon}",
      "format-icons": {
        "1": "",
        "2": "",
        "3": "",
        "4": "",
        "5": "",
        "6": "",
        "7": "",
        "8": "",
        "9": "",
        "10": "",
        "11": "",
        "12": "",
        "13": "",
        "14": "",
        "15": "",
        "16": "",
        "17": "",
        "18": "",
        "19": "",
        "20": "",
        "urgent": ""
        // "focused": "",
        // "default": ""
      },
      "persistent-workspaces": {
        "*": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
      }
    },
    "idle_inhibitor": {
      "format": "{icon}",
      "format-icons": {
        "activated": "",
        "deactivated": ""
      }
    },
    "clock": {
      // "timezone": "America/New_York",
      "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
      "format-alt": "{:%Y-%m-%d}"
    },
    "cpu": {
      "format": "󰍛",
      "format-alt": "󰍛 {usage}%",
      "tooltip": true
    },
    "memory": {
      "format-alt": " {used:.2} GiB",
      "format": "",
      "tooltip": true
    },
    "temperature": {
      "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
      "critical-threshold": 80,
      "format-critical": "{temperatureC}°C {icon}",
      "format": "{icon}",
      "format-icons": ["", "", "", "", ""],
      "tooltip": true
    },
    "backlight": {
      // "device": "acpi_video1",
      "format": "{icon}",
      "format-icons": ["󰪞", "󰪟", "󰪠", "󰪡", "󰪢", "󰪣", "󰪤", "󰪥"]
    },
    "battery": {
      "states": {
        // "good": 95,
        "warning": 30,
        "critical": 15
      },
      "format": "{icon}",
      "format-charging": "",
      "format-plugged": "",
      "format-alt": "{time} {icon}",
      // "format-good": "", // An empty format will hide the module
      // "format-full": "",
      "format-icons": ["󰂃", "󰁺", "󰁻", "󰁼", "󰁽", "󰁾", "󰁿", "󰂀", "󰂁", "󰂂", "󱈏"]
    },
    "battery#bat2": {
      "bat": "BAT2"
    },
    "network": {
      // "interface": "wlp2*", // (Optional) To force the use of this interface
      "format-wifi": "",
      "format-ethernet": " {ifname}: {ipaddr}/{cidr}",
      "format-linked": " {ifname} (No IP)",
      "format-disconnected": "",
      "format-alt": " {essid}: {ipaddr}",
      "tooltip-format": "{essid}: {ipaddr}"
    },
    "custom/pipewire": {
      "format": "{icon}",
      "return-type": "json",
      "signal": 8,
      "interval": "once",
      "format-icons": {
        "headphone": "",
        "hands-free": "",
        "headset": "",
        "phone": "",
        "portable": "",
        "car": "",
        "mute": "󱃓",
        "default": ["", "󰪞", "󰪟", "󰪠", "󰪡", "󰪢", "󰪣", "󰪤", "󰪥"]
      },
      "on-scroll-up": "wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+; pkill -RTMIN+8 waybar",
      "on-scroll-down": "wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%-; pkill -RTMIN+8 waybar",
      "on-click": "wpctl set-mute @DEFAULT_SINK@ toggle",
      //"exec": "echo \\\"{\"percentage\": 7, \"tooltip\": \"hejsa\"}\""
      //"exec": "${pkgs.pw-volume}/bin/pw-volume status"
      //"exec": "echo \"{\\\"percentage\\\": $(wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -o '[0-9.]\\+')}\"",
      "exec": "get-volume-json"
    },
    // "pulseaudio": {
    //   "format": "{icon}",
    //   "format-muted": "",
    //   "format-source": "{volume}% ",
    //   "format-source-muted": "",
    //   "format-icons": {
    //     "headphone": "",
    //     "hands-free": "",
    //     "headset": "",
    //     "phone": "",
    //     "portable": "",
    //     "car": "",
    //     "default": ["󰪞", "󰪟", "󰪠", "󰪡", "󰪢", "󰪣", "󰪤", "󰪥"]
    //   },
    //   "on-click": "pactl set-sink-mute @DEFAULT_SINK@ toggle",
    //   "format-bluetooth": "{icon} {format_source}",
    //   "format-bluetooth-muted": " {icon} {format_source}"
    // },
    "tray": {
            "icon-size":21
    }
  }
''
