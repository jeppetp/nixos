{ pkgs ? import <nixpkgs> {} }:
{
  doom-emacs = (builtins.getFlake "github:nix-community/nix-doom-emacs/34d8f65194514c041e7286215c52cf58ae682314").outputs.package.x86_64-linux
     { doomPrivateDir = ./doom.d;
       emacsPackagesOverlay = final: prev: {
          gptel =
            (final.melpaBuild rec {
              pname = "gptel";
              version = "0.4.0";
              commit = "0f161a4";
              src = pkgs.fetchFromGitHub {
                owner = "karthink";
                repo = "gptel";
                rev = commit;
                sha256 = "sha256-9mDSwxKs49NmbhRsv23mOqAtRsWwGrDVdv5DpJsETKk=";
              };
              packageRequires = [ ];
              recipe = pkgs.writeText "recipe" ''
                (gptel :repo "karthink/gptel" :fetcher github)
              '';
            });
       };
  };
}
