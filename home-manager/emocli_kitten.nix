{ pkgs ? import <nixpkgs> {} }:


let
  repo = pkgs.fetchFromGitHub {
    owner = "kofoednielsen";
    repo = "dotfiles";
    rev = "e48fe46";
    sha256 = "k+/k8/93hDFi9R8Ltw/PKfR+hiC3Xb70hZoPctf+KLc=";
  };
in
pkgs.stdenv.mkDerivation {
  name = "emocli_kitten";
  src = ./.;
  installPhase = ''
    mkdir $out
    cp emocli_kitten.py $out/emocli_kitten.py
  '';
}
