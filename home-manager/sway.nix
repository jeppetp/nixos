{ colors ? import ./colors.nix
, pkgs ? import <nixpkgs> {} }:

let
   mod = "Mod4";
   left = "h";
   down = "j";
   up = "k";
   right = "l";
in
{
  enable = true;
  config = {
    output = {
      "eDP-1" = {
        pos = "0 0";
        scale = "1";
        mode = "1920x1080@60Hz";
	bg = "${./bg} fill";
      };
      "DP-4" = {
        pos = "3200 0";
	mode = "1920x1080@60Hz";
	bg = "${./bg} fill";
      };
      "DP-6" = {
        pos = "5120 0";
	mode = "1920x1080@60Hz";
	bg = "${./bg} fill";
      };
    };

    input = {
      "*" = {
        # keyboard layout
        xkb_layout = "us";
        xkb_variant = "altgr-intl";
        xkb_options = "caps:super";

        # trackpad settings
        tap = "enabled";
        natural_scroll = "enabled";
        dwt = "disabled";
      };
    };

    gaps = {
      outer = 0;
      inner = 15;
    };

    window = {
      titlebar = false;
      border = 0;
    };

    colors = {
      background = colors.background;
      focused = {
        border = colors.color5;
	childBorder = colors.color5;
	indicator = colors.color5;
	background = colors.background;
	text = colors.foreground;
      };
      focusedInactive = {
        border = colors.background;
	childBorder = colors.background;
	indicator = colors.background;
	background = colors.background;
	text = colors.foreground;
      };
      unfocused = {
        border = colors.background;
	childBorder = colors.background;
	indicator = colors.background;
	background = colors.background;
	text = colors.foreground;
      };
      urgent = {
        border = colors.color1;
	childBorder = colors.background;
	indicator = colors.background;
	background = colors.background;
	text = colors.foreground;
      };
    };

    bars = [{
      fonts = {
        names = [ "Inconsolata" ];
        size = 24.0;
      };
      # statusCommand = 
      command = "waybar";
      colors = {
        background = colors.background;
        statusline = colors.foreground;
        separator = colors.color0;
        focusedWorkspace = {
          border = colors.foreground;
          background = colors.foreground;
          text = colors.background;
        };
        activeWorkspace = {
          border = colors.foreground;
          background = colors.background;
          text = colors.foreground;
        };
        inactiveWorkspace = {
          border = colors.background;
          background = colors.background;
          text = colors.foreground;
        };
        urgentWorkspace = {
          border = colors.color1;
          background = colors.color1;
          text = colors.background;
        };
        bindingMode = {
          border = colors.color2;
          background = colors.color2;
          text = colors.background;
        };
      };
    }];

    modifier = mod;
    keybindings = {
      "${mod}+t" = "exec kitty";
      "${mod}+e" = "exec emacsclient -c";
      "${mod}+i" = "exec emacsclient --eval \"(emacs-everywhere)\"";
      "${mod}+Return" = "exec swaylock";
      "${mod}+Shift+Return" = "exec qutebrowser";
      "${mod}+Shift+q" = "kill";
      "${mod}+d" = "exec wofi -S run --style ~/.config/wofi/style.css";
      "${mod}+b" = "exec brightnessctl set +2%";
      "${mod}+Shift+b" = "exec brightnessctl set 2%-";
      "${mod}+p" = "exec ${pkgs.grim}/bin/grim -g \"$(${pkgs.slurp}/bin/slurp -d)\" - | ${pkgs.wl-clipboard}/bin/wl-copy ";

      "${mod}+Shift+minus" = "move scratchpad";
      "${mod}+minus" = "scratchpad show";


      # focus
      "${mod}+${left}" = "focus left";
      "${mod}+${down}" = "focus down";
      "${mod}+${up}" = "focus up";
      "${mod}+${right}" = "focus right";

      "${mod}+Left" = "focus left";
      "${mod}+Down" = "focus down";
      "${mod}+Up" = "focus up";
      "${mod}+Right" = "focus right";

      # move windows
      "${mod}+Shift+${left}" = "move left";
      "${mod}+Shift+${down}" = "move down";
      "${mod}+Shift+${up}" = "move up";
      "${mod}+Shift+${right}" = "move right";

      "${mod}+Shift+Left" = "move left";
      "${mod}+Shift+Down" = "move down";
      "${mod}+Shift+Up" = "move up";
      "${mod}+Shift+Right" = "move right";

      "${mod}+Shift+ctrl+${left}" = "move workspace to output left";
      "${mod}+Shift+ctrl+${right}" = "move workspace to output right";
      "${mod}+Shift+ctrl+${up}" = "move workspace to output up";
      "${mod}+Shift+ctrl+${down}" = "move workspace to output down";

      # extra window options
      "${mod}+g" = "splith";
      "${mod}+v" = "splitv";
      "${mod}+f" = "fullscreen toggle";
      "${mod}+a" = "focus parent";

      "${mod}+q" = "layout stacking";
      "${mod}+w" = "layout tabbed";
      "${mod}+s" = "layout toggle split";

      "${mod}+Shift+space" = "floating toggle";
      "${mod}+space" = "focus mode_toggle";

      # reload config
      "${mod}+Shift+r" = "reload";
      "${mod}+Shift+e" =
        "exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";

      # resizing
      "${mod}+r" = "mode resize";

      # switch to workspace
      "${mod}+1" = "workspace number 1";
      "${mod}+2" = "workspace number 2";
      "${mod}+3" = "workspace number 3";
      "${mod}+4" = "workspace number 4";
      "${mod}+5" = "workspace number 5";
      "${mod}+6" = "workspace number 6";
      "${mod}+7" = "workspace number 7";
      "${mod}+8" = "workspace number 8";
      "${mod}+9" = "workspace number 9";
      "${mod}+0" = "workspace number 10";
      "${mod}+ctrl+1" = "workspace number 11";
      "${mod}+ctrl+2" = "workspace number 12";
      "${mod}+ctrl+3" = "workspace number 13";
      "${mod}+ctrl+4" = "workspace number 14";
      "${mod}+ctrl+5" = "workspace number 15";
      "${mod}+ctrl+6" = "workspace number 16";
      "${mod}+ctrl+7" = "workspace number 17";
      "${mod}+ctrl+8" = "workspace number 18";
      "${mod}+ctrl+9" = "workspace number 19";
      "${mod}+ctrl+0" = "workspace number 20";
      
      # move focused container to workspace
      "${mod}+Shift+1" = "move container to workspace number 1";
      "${mod}+Shift+2" = "move container to workspace number 2";
      "${mod}+Shift+3" = "move container to workspace number 3";
      "${mod}+Shift+4" = "move container to workspace number 4";
      "${mod}+Shift+5" = "move container to workspace number 5";
      "${mod}+Shift+6" = "move container to workspace number 6";
      "${mod}+Shift+7" = "move container to workspace number 7";
      "${mod}+Shift+8" = "move container to workspace number 8";
      "${mod}+Shift+9" = "move container to workspace number 9";
      "${mod}+Shift+0" = "move container to workspace number 10";
      "${mod}+ctrl+Shift+1" = "move container to  workspace number 11";
      "${mod}+ctrl+Shift+2" = "move container to  workspace number 12";
      "${mod}+ctrl+Shift+3" = "move container to  workspace number 13";
      "${mod}+ctrl+Shift+4" = "move container to  workspace number 14";
      "${mod}+ctrl+Shift+5" = "move container to  workspace number 15";
      "${mod}+ctrl+Shift+6" = "move container to  workspace number 16";
      "${mod}+ctrl+Shift+7" = "move container to  workspace number 17";
      "${mod}+ctrl+Shift+8" = "move container to  workspace number 18";
      "${mod}+ctrl+Shift+9" = "move container to  workspace number 19";
      "${mod}+ctrl+Shift+0" = "move container to  workspace number 20";
    };
  };
}

