{ pkgs ? import <nixpkgs> {} }:

let
  repo = pkgs.fetchFromGitLab {
    owner = "asbjornolling";
    repo = "emocli";
    rev = "main";
    sha256 = "cZ+eQLDmTjK/6ZNItBM1vP6ac7BLxl+/OlBl/i+ZZxI=";
  };
in

(import "${repo}/default.nix" {}).emocli
