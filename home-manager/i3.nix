{ config, lib, pkgs, colors ? import ./colors.nix, ...}:

let 
  mod = "Mod4";
  term = "${pkgs.kitty}/bin/kitty";
in {
  xsession.windowManager.i3 = {
    # package = pkgs.i3-gaps;
    enable = true;
    config = {
      modifier = mod;

      fonts = ["DejaVu Sans Mono, FontAwesome 6"];

      keybindings = lib.mkOptionDefault {
        "${mod}+p" = "exec ${pkgs.dmenu}/bin/dmenu_run";
        "${mod}+x" = "exec sh -c '${pkgs.maim}/bin/maim -s | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png'";
        "${mod}+Shift+x" = "exec sh -c '${pkgs.i3lock}/bin/i3lock -c 222222 & sleep 5 && xset dpms force of'";
        "${mod}+t" = "exec --no-startup-id ${term}";
        "${mod}+e" = "exec --no-startup-id emacsclient -c";

        # Focus
        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        # Move
        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        # multi monitor setup
        "${mod}+m" = "move workspace to output DVI-I-1-1";
        "${mod}+Shift+m" = "move workspace to output DVI-I-2-2";

        # Use pactl to adjust volume in PulseAudio.
        "XF86AudioRaiseVolume" = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status";
        "XF86AudioLowerVolume" = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status";
        "XF86AudioMute" = "exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status";
        "XF86AudioMicMute" = "exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status";
        "Print" = "exec --no-startup-id xfce4-screenshooter";

        # enable brightness adjustment buttons
        "XF86MonBrightnessUp"  = "exec --no-startup-id ${pkgs.brightnessctl}/bin/brightnessctl set +3%";
        "XF86MonBrightnessDown" = "exec --no-startup-id ${pkgs.brightnessctl}/bin/brightnessctl set 3%-";
      };
     
      gaps = {
        inner = 10;
      };
      
      window.titlebar = false;
      window.border = 0;

      bars = [
        {
          position = "top";
          statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-top.toml";
        }
      ];
    };
  };
}
