{ colors ? import ./colors.nix }:

''
  * {
    outline: none;
    box-shadow: unset;
    border: none;
  }
  #input:selection {
    background: rgba(1,1,1,0.5);
    color: rgba(0,0,0,0.5);
  }
  window {
    padding: 40px;
    font-family: "FiraCode Nerd Font Mono", "Font Awesome 5 Free";
    font-size: 30px;
    margin: 0px;
    background-color: rgba(0, 0, 0, 0.5);
    color: rgba(1, 1, 1, 0.5);
    border-radius: 20px;
    border: none;
  }

  #input {
    margin: 5px;
    border: none;
    outline: none;
    color: #f8f8f2;
    background-color: transparent;
  }

  input:focus {
    outline: none;
    outline-width: 0;
    border: none;
  }

  #inner-box {
    margin: 5px;
    border: none;
    background-color: transparent;
  }

  #outer-box {
    margin: 5px;
    border: none;
    background-color: transparent;
  }

  #scroll {
    margin: 0px;
    border: none;
  }

  #text {
    margin: 5px;
    border: none;
    color: #c5c8c6;
  }

  #entry {
    border: none;
    color: rgba(1, 1, 1, 0.3);
  }

  #entry:focus {
    border: none;
  }

  #entry:selected {
    background: transparent;
    border-radius: 20px;
    border: none;
    color: rgba(1, 1, 1, 1);
  }
''
