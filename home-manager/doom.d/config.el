(setq display-line-numbers-type 'relative)


(setq-default left-margin-width 1)
(setq-default right-margin-width 1)

(setq-default flycheck-indication-mode 'left-margin)

(setq doom-modules-dir (car doom-modules-dirs)
      doom-docs-dir (car doom-modules-dirs))

;; black - python autoformatting
(use-package! python-black
  :demand t
  :after python)
(add-hook! 'python-mode-hook #'python-black-on-save-mode)
(map! :leader :desc "Blacken Buffer" "m b b" #'python-black-buffer)
(map! :leader :desc "Blacken Region" "m b r" #'python-black-region)
(map! :leader :desc "Blacken Statement" "m b s" #'python-black-statement)

(map!
 :nv "g D" #'lsp-ui-peek-find-references
 :nv "g d" #'lsp-ui-peek-find-definitions
 :nv "g i" #'lsp-ui-peek-find-implementation
)

(setq doom-font (font-spec :family "Terminess Nerd Font" :size 24))
(setq doom-theme 'doom-dracula)

(setq gptel-api-key "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")
