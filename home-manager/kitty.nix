{ colors ? import ./colors.nix, pkgs ? import <nixpkgs> { } }:

let emocli_kitten = import ./emocli_kitten.nix { };
in {
  enable = true;

  # font
  # font = {
  #   package = pkgs.monocraft;
  #   name = "Monocraft Nerd Font";
  # };

  settings = {
    font_size = 25;
    font_hinting = "none";
    font_rendering = "high_quality";
    font_antialiasing = "enabled";

    background = colors.black;
    foreground = colors.foreground;
    background_opacity = "0.7";
    window_padding_width = "10";
    # selection_background = colors.color5;
    # selection_foreground = colors.color0;
    # url_color = colors.color4;
    cursor = colors.cursorColor;
    # active_border_color = colors.color3;
    # inactive_border_color = colors.color1;
    # active_tab_background = colors.color0;
    # active_tab_foreground = colors.color5;
    # inactive_tab_background = colors.color1;
    # inactive_tab_foreground = colors.color4;
    # tab_bar_background = colors.color1;

    # normal
    color0 = colors.color0;
    color1 = colors.color1;
    color2 = colors.color2;
    color3 = colors.color3;
    color4 = colors.color4;
    color5 = colors.color5;
    color6 = colors.color6;
    color7 = colors.color7;

    # bright
    color8 = colors.color8;
    color9 = colors.color9;
    color10 = colors.color10;
    color11 = colors.color11;
    color12 = colors.color12;
    color13 = colors.color13;
    color14 = colors.color14;
    color15 = colors.color15;

  };
  # add line to end of config for emocli
  # map ctrl+shift+u kitten ${emocli_kitten}/emocli_kitten.py
  extraConfig = ''
    confirm_os_window_close 2
    map ctrl+shift+u kitten ${emocli_kitten}/emocli_kitten.py
    font_family Monocraft Nerd Font
    fallback_family FiraMono Nerd Font, Noto Color Emoji
  '';
}
