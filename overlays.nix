self: super:

let
  java21 = super.pkgs.openjdk21;
in {
  prismlauncher = super.prismlauncher.overrideAttrs (oldAttrs: rec {
    buildInputs = (oldAttrs.buildInputs or []) ++ [ java21 ];
  });
}
