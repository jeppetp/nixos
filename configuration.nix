# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, callPackage, ... }:

let
  colors = import ./colors.nix;
  myOverlays = import ./overlays.nix;
  minegrub = builtins.getFlake "github:Lxtharia/minegrub-theme";
  brr = builtins.getFlake "gitlab:p6s/brr";
in {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    <home-manager/nixos>
    minegrub.nixosModules.default
    (builtins.getFlake "gitlab:p6s/vi/f791d42a16207a109819613d858212af394d6634").nixosModules.default
  ];
  nixpkgs.config.allowBroken = true;
  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.systemd-boot.configurationLimit = 50;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub = {
    enable = true;
    useOSProber = true;
    # device = "/dev/nvme0n1";
    efiSupport = true;
    configurationLimit = 50;
    devices = [ "nodev" ];
    minegrub-theme = {
      enable = true;
      splash = "lol";
      boot-options-count = 4;
    };
  };

  boot.extraModulePackages = with config.boot.kernelPackages; [ evdi ];

  # networking :D
  networking.hostName = "shit"; # Define your hostname.
  networking.networkmanager.enable =
    true; # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/Copenhagen";
  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    font = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";
    keyMap = "us";
  };
  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;
  # hardware.pulseaudio.extraConfig = "unload-module module-suspend-on-idle";
  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez;
  };

  systemd.services.keyd.enable = true;

  #systemd.services.keyd = {
  #  description = "Key remapping daemon";
  #  enable = true;
  #  serviceConfig = {
  #    Type = "simple";
  #    ExecStart = "${pkgs.keyd}/bin/keyd";
  #  };
  #  wantedBy = [ "sysinit.target" ];
  #  requires = [ "local-fs.target" ];
  #  after = [ "local-fs.target" ];
  #};
  powerManagement.enable = true;
  services.thermald.enable = true;
  services.tlp.enable = true;


  services.blueman.enable = true;
  services.hardware.bolt.enable = true;
  services.keyd.enable = true;
  xdg.portal.enable = true;
  services.flatpak.enable = true;
  virtualisation.docker.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.

  users.users.jeppe = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "audio"
      "docker"
      "wireshark"
      "networkmanager"
    ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
  };
  environment.etc = {
    "keyd/default.conf" = {
      text = ''
        # NOTE: to use this, rename this file to default.conf and put in /etc/keyd/

        # Advanced use of capslock as a triple function key:
        #
        # - when 'capslock' is tapped (pressed + released), it behaves as ESC key
        #
        # - when 'capslock' is held, and used in combination with another key, it
        #   behaves like a 'super' key modifier (just like xcape)
        #
        # - when 'capslock' is held, and the 'space' key is tapped, we enter a 3rd
        #   state for "VIM mode" where hjkl keys become arrow keys until capslock
        #   is released.
        #
        [ids]
        *

        [main]

        capslock = overload(super_vim, esc)

        # super_vim modifier layer; inherits from 'Super' modifier layer

        [super_vim:M]

        space = swap(vim_mode)

        # vim_mode modifier layer; also inherits from 'Super' modifier layer

        [vim_mode:M]

        h = left
        j = down
        k = up
        l = right
        # forward word
        w = C-right
        # backward word
        b = C-left
      '';
      mode = "0440";
    };
  };
  environment.sessionVariables = { QT_SCREEN_SCALE_FACTORS = "eDP-1=2"; };
  programs.fish.enable = true;
  programs.steam.enable = true;
  programs.openvpn3.enable = true;

  programs.sway = {
   enable = true;
   extraPackages = with pkgs; [swaylock swayidle waybar xwayland ];
  };

  # HACKER UR HACKER UR HACKER UR HACKER UR
  services.udev.packages = [
    (pkgs.writeTextFile {
      name = "probe-rs udev rules";
      text = ''
        ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE="660", GROUP="users", TAG+="uaccess"'';
      destination = "/etc/udev/rules.d/69-probe-rs.rules";
    })
  ];

  home-manager.users.jeppe = { pkgs, ... }: {
    home.stateVersion = "23.05";
    # home.pointerCursor = {
    #   gtk.enable = true;
    #   package = pkgs.bibata-cursors;
    #   name = "Bibata-Modern-Ice";
    #   size = 35;
    # };

    imports = [ ./home-manager/i3.nix ];

    gtk = {
      enable = true;
      theme = {
        name = "Breeze-Dark";
        package = pkgs.libsForQt5.breeze-gtk;
      };
    };

    programs.fish = import ./home-manager/fish.nix { };
    programs.swaylock = import ./home-manager/swaylock.nix { };
    programs.starship = {
      enable = true;
      enableFishIntegration = true;
      enableTransience = true;
      settings = {
        character = {
          success_symbol = "[>](bold green)";
          error_symbol = "[>](bold red)";
          vimcmd_symbol = "[<](bold green)";
          vimcmd_replace_one_symbol = "[<](bold purple)";
          vimcmd_replace_symbol = "[<](bold purple)";
          vimcmd_visual_symbol = "[<](bold yellow)";

        };
      };
    };
    services.dunst = import ./home-manager/dunst.nix { };
    programs.kitty = import ./home-manager/kitty.nix { };
    programs.qutebrowser = import ./home-manager/qutebrowser.nix { pkgs = pkgs; };

    # home.file = {
    #   ".config/lvim/config.lua".source = ./home-manager/lvim/config.lua;
    # };


    # sway stuff
    # wayland.windowManager.sway = import ./home-manager/sway.nix {};

    wayland.windowManager.hyprland = import ./home-manager/hyprland.nix { };
    programs.waybar.enable = true;

    xdg.configFile = {
      "waybar/config".text =
        import ./home-manager/waybar_config.nix { pkgs = pkgs; };
      "waybar/style.css".text = import ./home-manager/waybar_style.nix { };
      "wofi/style.css".text = import ./home-manager/wofi_style.nix { };
    };
  };

  fonts.packages = with pkgs; [
    noto-fonts
    monocraft
    noto-fonts-cjk-sans
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    mplus-outline-fonts.githubRelease
    dina-font
    proggyfonts
    aileron # YEAH HELVETICA
    # font-awesome # some icons
    inconsolata # decent font
    terminus_font
    terminus_font_ttf
    eb-garamond
  ];

  nixpkgs.config = {
    allowUnfree = true;
    pulseaudio = true;
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nixpkgs.overlays = [ 
    myOverlays 
    (final: prev: {
      nixvim = (import (builtins.getFlake "github:NixOS/nixpkgs-unstable") {}).typescript-language-server;
    })
    ];

  services.mullvad-vpn.enable = true;

  environment.systemPackages = with pkgs; [
    mullvad-vpn
    mpv
    wlr-randr
    hyprland
    gcc # du må få den når jeg dør
    qbittorrent
    brr.packages.x86_64-linux.default
    # ghc
    pw-volume
    brightnessctl
    cabal-install
    xdg-utils # xdg-open and stuff
    vim # nice
    # unstable.qutebrowser      # for browsing web
    sshx
    # idris
    kitty # mu
    wofi # launcher, dmenu alternative
    git # version control
    nheko # matrix client
    ripgrep # better grep
    fd
    clang
    dconf
    coreutils
    gnupg
    cmake
    wl-clipboard
    dunst
    libnotify
    # unstable.lunarvim
    hyprpaper # hyprland wallpaper
    keyd
    element-desktop-wayland
    (writeShellScriptBin "battery-notify" ''
    battery_level=`${pkgs.acpi}/bin/acpi -b | grep -P -o '[0-9]+(?=%)'`
    charging_status=`${pkgs.acpi}/bin/acpi -b | grep -P -o ': Charging, '`
    if [ $battery_level -le 20 ] && [ -n $charging_status ]
    then
      export DISPLAY=:0
      notify-send "Battery low" "Battery level is ''${battery_level}"
    fi
    '')
    (writeShellScriptBin "cstimer" "chromium cstimer.net --kiosk")
    (writeShellScriptBin "beeper" "export GDK_SCALE=2 && ${pkgs.beeper}/bin/beeper")
    (writeShellScriptBin "steam" "export GDK_SCALE=2 && ${pkgs.steam}/bin/steam")
    (writeShellScriptBin "get-volume-json" ''
      status=$(wpctl get-volume @DEFAULT_AUDIO_SINK@)
      percentage=$(echo $status | awk -v RS='[0-9]+' '$0=RT' | tr --delete '\n')
      if muted="$(echo $status | grep 'MUTED')" ; then
          echo "{\"alt\":\"mute\", \"tooltip\":\"muted\", \"class\":\"muted\"}"
      else
          echo "{\"percentage\":$percentage, \"tooltip\":\"yoyo\"}"
               fi
    '')
    (writeShellScriptBin "rebuild" "sudo nixos-rebuild switch --log-format internal-json |& ${pkgs.nix-output-monitor}/bin/nom --json")
    (writeShellScriptBin "torrent" "${pkgs.transmission_4}/bin/transmission-cli $(wl-paste)")

    chromium # for web bluetooth
    gnumake
    dockfmt
    nixfmt
    black
    iamb

    isort
    lsd # better ls
    bat # better cat
    pango # something something fonts
    # displaylink      # dell docks
    # autorandr        # automatic monitor config when plugged in (i think....)
    gotop # nice process monitor
    pavucontrol # GUI pulsemixer
    direnv # folder-based environment
    starship # nice, fast prompt
    p7zip # more bits per byte
    btop # better top (with nice escape menu)
    python312
    python311 # 🐍
    python310
    python311Packages.pip # python packagesssss
    docker
    comma # use non-installed command once
    prismlauncher
    wireplumber
    pulseaudio
    emocli
  ];

  # dell d6000
  # services.xserver.videoDrivers = [ "displaylink" "modesetting" ];
  # services.xserver.videoDrivers = [ "modesetting" ];

  services = {
    gnome.gnome-keyring.enable = true;
    # autorandr.enable = true;
  };
  security.pam.services.swaylock = { };
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };
  # services.cron = {
  #   enable = true;
  #   systemCronJobs = [
  #     "*/1 * * * *  jeppe battery-notify"
  #   ];
  # };
  #
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 1337 1234 13337];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

